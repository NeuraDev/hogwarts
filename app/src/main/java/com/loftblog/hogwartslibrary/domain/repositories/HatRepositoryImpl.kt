package com.loftblog.hogwartslibrary.domain.repositories

import com.loftblog.hogwartslibrary.data.network.RetrofitFactory
import com.loftblog.hogwartslibrary.domain.models.FacultyModel
import kotlinx.coroutines.delay
import retrofit2.Retrofit

class HatRepositoryImpl: HatRepository {

    override suspend fun generateFaculty(username: String): FacultyModel {
        return FacultyModel(name = RetrofitFactory.instance.housesService.sortingHat(key = RetrofitFactory.key))
    }
}