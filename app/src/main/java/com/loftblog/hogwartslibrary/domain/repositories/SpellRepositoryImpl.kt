package com.loftblog.hogwartslibrary.domain.repositories

import com.loftblog.hogwartslibrary.data.network.RetrofitFactory
import com.loftblog.hogwartslibrary.domain.models.SpellModel
import com.loftblog.hogwartslibrary.domain.models.mapToDomain

class SpellRepositoryImpl: SpellRepository {

    override suspend fun getAllSpells(): List<SpellModel> {
        return RetrofitFactory.instance.spellService.getAllSpells(key = RetrofitFactory.key)
            .map { it.mapToDomain() }
    }
}