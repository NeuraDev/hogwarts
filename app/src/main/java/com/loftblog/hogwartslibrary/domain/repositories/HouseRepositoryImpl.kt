package com.loftblog.hogwartslibrary.domain.repositories

import com.loftblog.hogwartslibrary.data.network.RetrofitFactory
import com.loftblog.hogwartslibrary.data.services.HousesService
import com.loftblog.hogwartslibrary.domain.models.HouseModel
import com.loftblog.hogwartslibrary.domain.models.mapToDomain
import com.loftblog.hogwartslibrary.ui.scenes.house.Houses
import java.lang.Exception

class HouseRepositoryImpl: HouseRepository {

    override suspend fun getHouseDetails(house: Houses): HouseModel? {
        val houseId = when (house) {
            Houses.Gryffindor -> HousesService.griffindorId
            Houses.Hufflepuff -> HousesService.hufflepuffId
            Houses.Ravenclaw -> HousesService.ravenclawId
            Houses.Slytherin -> HousesService.slytherinId
        }

        return try {
            RetrofitFactory.instance.housesService.getHouseDetails(key = RetrofitFactory.key,
                houseId = houseId).map { it.mapToDomain() }[0]
        } catch (e: Exception) {
            return null
        }
    }
}