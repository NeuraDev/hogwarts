package com.loftblog.hogwartslibrary.domain.repositories

import com.loftblog.hogwartslibrary.domain.models.SpellModel

interface SpellRepository {
    suspend fun getAllSpells(): List<SpellModel>
}