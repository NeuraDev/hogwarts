package com.loftblog.hogwartslibrary.domain.models

import com.loftblog.hogwartslibrary.data.models.SpellRemote

data class SpellModel(val id: String, val name: String, val type: String)

fun SpellRemote.mapToDomain(): SpellModel {
    return SpellModel(id = this._id, name = this.spell, type = this.type)
}