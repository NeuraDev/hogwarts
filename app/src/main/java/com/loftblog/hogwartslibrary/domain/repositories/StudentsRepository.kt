package com.loftblog.hogwartslibrary.domain.repositories

import com.loftblog.hogwartslibrary.domain.models.StudentModel
import com.loftblog.hogwartslibrary.ui.scenes.students.adapters.StudentCellModel

interface StudentsRepository {
    suspend fun fetchStudents(): List<StudentModel>?
}