package com.loftblog.hogwartslibrary.data.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SpellRemote(
    val _id: String, val spell: String = "", val type: String = "", val effect: String = "",
    @SerialName("__v") val version: Int = 0
)