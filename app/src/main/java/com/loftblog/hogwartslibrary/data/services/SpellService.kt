package com.loftblog.hogwartslibrary.data.services

import com.loftblog.hogwartslibrary.data.models.CharacterRemote
import com.loftblog.hogwartslibrary.data.models.SpellRemote
import retrofit2.http.GET
import retrofit2.http.Query

interface SpellService {

    @GET("./spells")
    suspend fun getAllSpells(@Query("key") key: String): List<SpellRemote>
}