package com.loftblog.hogwartslibrary.data.network

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.loftblog.hogwartslibrary.data.services.CharactersService
import com.loftblog.hogwartslibrary.data.services.HousesService
import com.loftblog.hogwartslibrary.data.services.SpellService
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit

class RetrofitFactory {

    companion object {
        val key = "\$2a\$10\$2vumovSp20jh0AqNzAPVluvxcyQzQopzL5pGxzhGkg08ncF/Q77Li"
        val instance = RetrofitFactory()
    }

    private fun okHttpInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    private val okHttpClient = OkHttpClient.Builder()
        .addInterceptor(okHttpInterceptor())
        .build()

    private val retrofitClient: Retrofit = Retrofit.Builder()
        .baseUrl("https://www.potterapi.com/v1/")
        .client(okHttpClient)
        .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
        .build()

    val charactersService: CharactersService = retrofitClient.create(CharactersService::class.java)
    val housesService: HousesService = retrofitClient.create(HousesService::class.java)
    val spellService: SpellService = retrofitClient.create(SpellService::class.java)
}