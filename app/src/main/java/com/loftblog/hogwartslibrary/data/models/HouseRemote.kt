package com.loftblog.hogwartslibrary.data.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class HouseRemote(val _id: String, val name: String = "", val mascot: String = "", val headOfHouse: String = "",
                       val houseGhost: String = "", val founder: String = "", @SerialName("__v") val version: Int,
                       val school: String = "", val members: List<HouseMember>, val values: List<String>,
                       val colors: List<String>)
@Serializable
data class HouseMember(val _id: String, val name: String = "")