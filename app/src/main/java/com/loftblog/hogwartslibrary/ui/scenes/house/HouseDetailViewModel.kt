package com.loftblog.hogwartslibrary.ui.scenes.house

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.loftblog.hogwartslibrary.R
import com.loftblog.hogwartslibrary.domain.repositories.HouseRepositoryImpl
import com.loftblog.hogwartslibrary.domain.repositories.StudentsRepositoryImpl
import com.loftblog.hogwartslibrary.ui.scenes.students.adapters.mapToUI
import kotlinx.android.synthetic.main.fragment_houses.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HouseDetailViewModel : ViewModel() {

    private val housesRepository = HouseRepositoryImpl()

    private val _ghost: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }
    private val _founder: MutableLiveData<String> = MutableLiveData<String>().apply { value = "'" }
    private val _leader: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }
    private val _houseName: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }
    private val _houseImage: MutableLiveData<Int> = MutableLiveData<Int>().apply { value = R.drawable.img_gryffindor }

    val ghost: LiveData<String> = _ghost
    val founder: LiveData<String> = _founder
    val leader: LiveData<String> = _leader
    val houseName: LiveData<String> = _houseName
    val houseImage: LiveData<Int> = _houseImage

    fun fetchData(house: Houses?) {
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                house?.let {
                    val details = housesRepository.getHouseDetails(house = house)
                    _ghost.postValue(details?.ghost.orEmpty())
                    _founder.postValue(details?.founder.orEmpty())
                    _leader.postValue(details?.leader.orEmpty())
                    _houseName.postValue(details?.name.orEmpty())

                    when (house) {
                        Houses.Gryffindor -> _houseImage.postValue(R.drawable.img_gryffindor)
                        Houses.Slytherin -> _houseImage.postValue(R.drawable.img_slytherin)
                        Houses.Ravenclaw -> _houseImage.postValue(R.drawable.img_ravenclaw)
                        Houses.Hufflepuff -> _houseImage.postValue(R.drawable.img_hufflepuff)
                    }
                }
            }
        }
    }
}